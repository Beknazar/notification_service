# Notification

# Start project:
  1) Start docker machine on your PC
  2) Change main project directory in terminal
  3) Create .env file near core directory by .env.example 
  4) Run command **"sudo docker-compose up --build"** in your terminal
  5) Go to localhost in your browser (without port 8000, nginx was connected)
  6) Project was successful started

# API
  1) Go to **http://localhost/api/client/** in Postman and set to body this
     - {
        "phone_number": Client phone_number,
        "mobile_operator_code": Client mobile operator code,
        "client_tag": "Client tag",
        "client_timezone": "Client timezone"
      }
     - Select POST method
     
  2) Go to **http://localhost/api/distribution/** in Postman and set to body this
     - {
        "start_date": "Mailing start date",
        "end_date": "Mailing end date",
        "text": "mailing message",
        "clients": [client_id]
      }
     - Select POST method
     
     
  3) Go to **http://localhost/api/distribution/** in Postman and set to body this
     - Select GET method 

  4) For start tests 
     - Run command **"sudo docker ps"** or **"sudo docker container ls"**(when project in docker started)
     - Seach container with name **web_notification** and copy his CONTAINER ID
     - Run command **"sudo docker exec -it *CONTAINER ID* python manage.py createsuperuser"**
