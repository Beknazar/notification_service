from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver

from django_celery_beat.models import ClockedSchedule, PeriodicTask

import json

import logging

logger = logging.getLogger(__name__)


class Client(models.Model):
    phone_number = models.IntegerField(verbose_name="Номер телефона")
    mobile_operator_code = models.IntegerField(verbose_name="Код мобильного оператора")
    client_tag = models.CharField(max_length=100, verbose_name="Тег")
    client_timezone = models.CharField(max_length=100)
    
    def __str__(self):
        return f"{self.phone_number}"
    
    
    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"


class Distribution(models.Model):
    start_date = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    end_date = models.DateTimeField(verbose_name="Дата и время окончания рассылки")
    text = models.CharField(max_length=1000, verbose_name="Сообщения для доставки клиенту")
    clients = models.ManyToManyField(
        Client, related_name="distributions", verbose_name="Клиенты"
        )

    def __str__(self):
        return self.text
    
    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время создания")
    status = models.BooleanField(default=False, verbose_name="Статус отправки")
    distribution = models.ForeignKey(
        Distribution, on_delete=models.CASCADE, related_name="messages", verbose_name="Рассылка"
    )
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="messages", verbose_name="Клиент"
    )
    is_send = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.status}"
    
    
    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"


@receiver(post_save, sender=Distribution)
def create_connected_models(sender, instance, created, **kwargs):
    logger.info(f"create distribution:   pk = {instance.pk}")
    
    clocked = ClockedSchedule.objects.create(
        clocked_time=instance.start_date
    )
    
    PeriodicTask.objects.create(
        name=f"{instance.pk} -- {instance.text[0]}",
        clocked=clocked,
        task="mainapp.tasks.send_message_to_phone_number",
        args=json.dumps([instance.pk, ]),
        start_time=instance.start_date,
        one_off=True,
    )


@receiver(post_save, sender=Client)
def create_logg_for_client(sender, instance, created, **kwargs):
    logger.info(f"create client:   pk = {instance.pk}")