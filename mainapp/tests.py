from rest_framework.test import APITestCase
from django.urls import reverse

from django.utils import timezone

from datetime import timedelta

from django_celery_beat.models import PeriodicTask, ClockedSchedule


class PatronTestCase(APITestCase):
    def setUp(self):
        data = {"phone_number": 704332220, "mobile_operator_code": 996, "client_tag": "Руководитель", "client_timezone": 6}
        data_2 = {"phone_number": 501332232, "mobile_operator_code": 996, "client_tag": "Заместитель", "client_timezone": 6}
        
        response = self.client.post(reverse("client-list"), data=data)
        response = self.client.post(reverse("client-list"), data=data_2)
        self.assertEqual(201, response.status_code)
    
    def test_create_client(self):
        response = self.client.get(reverse("client-list"))
        self.assertEqual(200, response.status_code)


class DistributionTestCase(APITestCase):
    def setUp(self):
        
        client_1 = {"phone_number": 704332220, "mobile_operator_code": 996, "client_tag": "Руководитель", "client_timezone": 6}
        client_2 = {"phone_number": 501332232, "mobile_operator_code": 996, "client_tag": "Заместитель", "client_timezone": 6}
        distribution = {'start_date': timezone.now() + timedelta(minutes=2), 'end_date': timezone.now() + timedelta(minutes=5), "text": "Оплатите кредит", "clients": [1, 2]}
        
        response = self.client.post(reverse("client-list"), data=client_1)
        self.assertEqual(201, response.status_code)
        response = self.client.post(reverse("client-list"), data=client_2)
        self.assertEqual(201, response.status_code)
        response = self.client.post(reverse('distribution-list'), data=distribution)
        self.assertEqual(201, response.status_code)


    def test_create_distribution(self):
        response = self.client.get(reverse('distribution-list'))
        self.assertEqual(200, response.status_code)
        self.assertEqual(ClockedSchedule.objects.count(), 1)
        self.assertEqual(PeriodicTask.objects.count(), 1)

