import logging

logger = logging.getLogger(__name__)


class ClientMixin:
    def update(self, request, *args, **kwargs):
        logger.info(f"update client:  pk = {kwargs.get('id')}")
        return super().update(request, *args, **kwargs)
    
    def partial_update(self, request, *args, **kwargs):
        logger.info(f"patrial update client:  pk = {kwargs.get('id')}")
        return super().partial_update(request, *args, **kwargs)
    
    def destroy(self, request, *args, **kwargs):
        logger.debug(f"destroy client:  pk = {kwargs.get('id')}")
        return super().destroy(request, *args, **kwargs)


class DistributionMixin:
    def update(self, request, *args, **kwargs):
        logger.info(f"update distribution:  pk = {kwargs.get('id')}")
        return super().update(request, *args, **kwargs)
    
    def partial_update(self, request, *args, **kwargs):
        logger.info(f"patrial update distribution:  pk = {kwargs.get('id')}")
        return super().partial_update(request, *args, **kwargs)
    
    def destroy(self, request, *args, **kwargs):
        logger.info(f"destroy distribution:  pk = {kwargs.get('id')}")
        return super().destroy(request, *args, **kwargs)
