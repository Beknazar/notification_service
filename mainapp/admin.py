from django.contrib import admin

from mainapp.models import Client, Distribution, Message


admin.site.register(Client)
admin.site.register(Distribution)
admin.site.register(Message)
