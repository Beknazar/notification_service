from django.urls import path
from rest_framework.routers import DefaultRouter as DR

from mainapp.views import ClientView, DistributionView


router = DR()

router.register("client", ClientView, basename="client")
router.register("distribution", DistributionView, basename="distribution")

urlpatterns = [
    
]

urlpatterns += router.urls