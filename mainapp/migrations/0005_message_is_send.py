# Generated by Django 4.1.2 on 2022-12-15 08:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0004_alter_client_mobile_operator_code_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='is_send',
            field=models.BooleanField(default=False),
        ),
    ]
