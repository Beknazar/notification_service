from rest_framework import serializers

from mainapp.models import Client, Distribution
from mainapp.tasks import send_message_to_phone_number


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            "id", "phone_number", "mobile_operator_code", "client_tag", "client_timezone"
        )


class DistributionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Distribution
        fields = (
            "id", "start_date", "end_date", "text", "clients"
        )

