from core.settings import ACCESS_TOKEN

from django.utils import timezone

from mainapp.models import Message

import requests

import logging


logger = logging.getLogger(__name__)


def send_http_request(distribution_pk, headers, data):
    try:
        response = requests.post(f'https://probe.fbrq.cloud/v1/send/{distribution_pk}', headers=headers, json=data)
        logger.info(f'distribution pk = {distribution_pk} send request to : https://probe.fbrq.cloud/v1/send/*, status: {response.status_code}')
        return response.status_code
    except:
        logger.error(f"distribution pk: {distribution_pk}, url=https://probe.fbrq.cloud/v1/send/{distribution_pk}, headers={headers}, data={data}")

def send_message(distribution, client):
    
    if distribution.end_date < timezone.now():
        return None
    
    headers={'Authorization': ACCESS_TOKEN}
    data={
            "id": distribution.pk,
            "phone_number": int(f"{client.mobile_operator_code}{client.phone_number}"),
            "text": distribution.text
        }
    status = send_http_request(headers=headers, data=data, distribution_pk=distribution.pk)
    
    message = Message.objects.create(
        status=True if status==200 else False,
        distribution=distribution,
        client=client
    )
    logger.info(f"create message pk: {message.pk}")



