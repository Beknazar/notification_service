from mainapp.mixins import ClientMixin, DistributionMixin
from mainapp.models import Client, Distribution
from rest_framework.viewsets import ModelViewSet
from mainapp.serializers import ClientSerializer, DistributionSerializer

class ClientView(ModelViewSet, ClientMixin):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class DistributionView(ModelViewSet, DistributionMixin):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer
