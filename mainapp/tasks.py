from celery import shared_task

from mainapp.models import Distribution

from mainapp.services import send_message

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from mainapp.models import Message

from core.settings import DEFAULT_ORGANIZATION_EMAIL


@shared_task
def send_message_to_phone_number(*distribution):
    
    distribution = Distribution.objects.filter(pk=distribution[0]).first()
    for client in distribution.clients.all():
        send_message(distribution, client)



@shared_task
def send_mail_to_email():
    message = Message.objects.filter(is_send=False)
    
    data = {
        "email": DEFAULT_ORGANIZATION_EMAIL,
        'message': message,
    }
    html_body = render_to_string('email.html', data)
    msg = EmailMultiAlternatives(subject='Здравствуйте', to=[DEFAULT_ORGANIZATION_EMAIL])
    msg.attach_alternative(html_body, 'text/html')
    msg.send()
    message.update(is_send=True)